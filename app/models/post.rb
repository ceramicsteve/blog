class Post < ActiveRecord::Base
  #relationship of Post is one to many Comments
  #relationship of Post to Comments is that they Comments are dependent on Posts
  #relationship of Post is destroyed Comments associated with Post are also Destroyed 
  has_many :comments, dependent: :destroy 
  
  #Validation 
  validates_presence_of :title
  validates_presence_of :body
  
end
